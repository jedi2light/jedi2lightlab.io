(ns jedi2light.gitlab.io.pages.projects
  (:require [clojure.string :as str]
            [re-frame.core :as reframe]
            [jedi2light.gitlab.io.subs :as subs]
            [jedi2light.gitlab.io.events :as events]
            [jedi2light.gitlab.io.layout.naved :as naved]
            [jedi2light.gitlab.io.chota :as chota]
            [jedi2light.gitlab.io.utils :as little-utils]
            [jedi2light.gitlab.io.components.pagination :as pagination]
            [jedi2light.gitlab.io.components.pagination.subs :as pag-subs]))

(defn page []
  (let [projects @(reframe/subscribe [::subs/projects])
        current-page-num @(reframe/subscribe [::pag-subs/current-page-number :projects])
        partitioned-projects (if (>= (count projects) 6) (vec (partition 6 projects)) projects)]
    [naved/layout {:active-nav :projects}
     [chota/grid  {:row true}
      [chota/grid {:md 12 :xs 12}
       [:input    {:placeholder "Search ..."
                   :value @(reframe/subscribe [::subs/gitlab-projects-filter-word])
                   :on-change #(reframe/dispatch [::events/set-gitlab-projects-filter-word (little-utils/target-val %)])}]]]
     [chota/grid  {:row true}
      (for [{:keys [id name description]} (if (>= (count projects) 6) (get partitioned-projects current-page-num) projects)]
       ^{:key (little-utils/string-format "project-%d" id)}
       [chota/grid {:md 6
                    :xs 12}
         [chota/card {} [:header
                         [:h3 name]]
                        [:p (let [words (str/split description #"[^\w]+")]
                              (if (>= (count words) 10)
                                (str/join " " (concat (take 10 words) ["..."])) description))]
          [:footer [chota/link-button {:href (little-utils/string-format "#/projects/%d" id) :color :primary} "Get More"]]]])]
     [chota/grid {:row true :utils [:is-center]}
      [chota/grid {:md 4 :sm 8 :xs 12 :utils [:is-center]}
       [pagination/component {:panel-id :projects :pg-count (if (>= (count projects) 6) (count partitioned-projects) 1)}]]]]))
