(ns jedi2light.gitlab.io.pages.projects-item
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            ["marked" :as marked.js]
            [jedi2light.gitlab.io.subs :as subs]
            [jedi2light.gitlab.io.events :as events]
            [jedi2light.gitlab.io.layout.naved :as naved]
            [jedi2light.gitlab.io.chota :as chota]
            [jedi2light.gitlab.io.utils :as little-utils]))

(defn page []
  (r/create-class
    {:component-did-mount
     (fn [_]
       (let [project-id
             (:project-id @(rf/subscribe [::subs/active-section-params]))]
         (if (not @(rf/subscribe [::subs/gitlab-repo-file-content project-id "README.md"]))
           (rf/dispatch [::events/load-gitlab-repo-file-content project-id "README.md"]))))
     :render
     (fn [_]
       (let [{:keys [project-id]}    @(rf/subscribe [::subs/active-section-params])
             {:keys [name
                     links
                     description]}   @(rf/subscribe [::subs/projects project-id])
             project-link            (-> links first :link)
             project-readme          @(rf/subscribe [::subs/gitlab-repo-file-content project-id "README.md"])
             project-readme-loading? @(rf/subscribe [::subs/gitlab-repo-file-loading? project-id "README.md"])]
         (prn links)
         (marked.js/setOptions
           ;; FIXME: why this does not work?????
           #js {:baseUrl project-link :gfm true})
         [naved/layout {:active-nav :projects-item}
          [chota/grid {:row true :utils [:is-center]}
           [chota/grid {:md 12 :xs 12}
            [chota/card {} [:header.text-grey [:h4 name]]
             (when project-readme
               [:div
                {:dangerouslySetInnerHTML
                 {:__html (marked.js (js/atob project-readme))}}])
             (when (and (not project-readme) (not project-readme-loading?)) [:div [:p description]])
             [:footer.bg-light
              (for [{:keys [link]} links]
                ^{:key link} [chota/link-button
                              {:href link :target :_blank :outline true :rel :noopener} "On Gitlab"])]]]]]))}))

