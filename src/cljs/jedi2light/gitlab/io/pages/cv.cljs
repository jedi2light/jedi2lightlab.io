(ns jedi2light.gitlab.io.pages.cv
  (:require [jedi2light.gitlab.io.layout.naved :as naved]
            [jedi2light.gitlab.io.chota :as chota]
            [jedi2light.gitlab.io.utils :as little-utils]))

(defn page []
  [naved/layout {:active-nav :biography}
   [chota/grid {:row true :utils [:is-center]}
    [:h1 "About Me"]]
   [chota/grid
    {:row true :style {:padding "0px 10px 0px 10px"}}
    [chota/grid {:md 4 :sm 12}
     [chota/card {}
      [:div
       [:img {:src "img/MyAvatar.webp" :alt "My Avatar"}]]]]
    [chota/grid {:md 8 :sm 12}
     [:div {:class "is-full-path"}
      [chota/card {}
       [:header
        [:h2
         [:span {:style {:color "#818181"}} "Carey Minaieva. "]
         (str (- (little-utils/with-current-year) 1998) " yo. Kyiv.")]]
       [:div
        [:h4 "Companies, positions, and projects I have been involved to"]
        [:ol
         [:li
          [:strong "2017 August till 2019 October"]
          " - Freshcode LTD. Junior DevOps Engineer.
          Projects deployment automation using existing company infrastructure."]
         [:li
          [:strong "2019 October till 2020 June"]
          " - Freshcode LTD. Junior Clojure Developer.
          Clojure & ClojureScript (Re-frame (which is React + Redux but for ClojureScript)) based CRM."]
         [:li 
          [:strong "2020 October till 2021 April"]
          " - NPC Ukrenergo. Junior Python 3 Developer.
          Centralized energy reports management web service authentication module using OpenLDAP database."]
         [:li 
          [:strong "2021 April till 2021 September"]
          " - Vector Software LTD. Middle Clojure Developer.
          Event ticket (mostly, but not only) sales tracking service."]
         [:li
          [:strong "2021 December till 2023 April"]
          " - UKEESS Software. Middle Clojure Developer. "
          "Places rent platform. Written in Clojure and Node.js mostly. Microservices architecture. NoSQL database."]]
        [:h4 "Technologies stack I'm experienced with"]
        [:p [:strong "Operating systems: "] "GNU/Linux, FreeBSD, macOS, MS Windows NT"]
        [:p [:strong "Languages: "]  "C, C++, Python 3, Clojure, ClojureScript, JavaScript"]
        [:p [:strong "Frameworks: "] "Flask, Django, Django REST Framework, Compojure, Re-frame"]
        [:p [:strong "Workflow: "] "Bitbucket, Github, Gitlab, Gitlab CI, Trello, Jira, Confluence, Notion, Shortcut"]
        [:p [:strong "Databases and Services: "] "OpenLDAP, PostgreSQL, MongoDB, Nginx, Docker, docker-compose"]]]]]]])
