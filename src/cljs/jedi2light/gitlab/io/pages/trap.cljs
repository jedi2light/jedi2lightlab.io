(ns jedi2light.gitlab.io.pages.trap
  (:require [jedi2light.gitlab.io.layout.naved :as naved]
            [jedi2light.gitlab.io.chota :as chota]))

(defn page []
  [naved/layout {}
   [chota/grid {:row true :utils [:is-center]}
    [:h1 "Sorry, but requested page was not found :("]]])
