(ns jedi2light.gitlab.io.blocks.footer
  (:require [re-frame.core :as rf]
            [jedi2light.gitlab.io.subs :as subs]
            [jedi2light.gitlab.io.utils :as utils]
            [jedi2light.gitlab.io.icon :as icon]
            [jedi2light.gitlab.io.chota :as chota]))

(defn- copyright-block [{:keys [name]}]
  [:small {:class (utils/with-classes "is-left")}
   (utils/string-format "© %s %d" name (utils/with-current-year))])

(defn- contact-block [{:keys [email-address]}]
  [:small {:class (utils/with-classes "is-right")}
   [:a {:href (utils/string-format "mailto: %s" email-address)} email-address]])

(defn- social-block [{:keys [networks]}]
  [:div {:class (utils/with-classes "is-right")}
   (for [{:keys [id link]} networks]
     ^{:key id}
     [:a {:href link :target "_blank" :rel "noopener"}
      [icon/icon-gram-svg {:icon id :provider :fontawesome :size 16 :spacing 8}]])])

(defn component [props & children]
  (let [{:keys [my-name my-email my-networks]} @(rf/subscribe [::subs/personal])]
    [chota/grid {:row true :md 12 :xs 12 :style {:margin-top 20}}
     [chota/grid {:md 6 :xs 6} [copyright-block         {:name my-name}]]
     [chota/grid {:md 4 :xs 4} [contact-block           {:email-address my-email}]]
     [chota/grid {:md 2 :xs 2} [social-block            {:networks my-networks}]]]))
