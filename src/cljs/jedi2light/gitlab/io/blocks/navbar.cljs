(ns jedi2light.gitlab.io.blocks.navbar
  (:require [re-frame.core :as rf]
            [jedi2light.gitlab.io.subs :as subs]
            [jedi2light.gitlab.io.utils :as utils]
            [jedi2light.gitlab.io.chota :as chota]))

(defn component [{:keys [active-nav]}]
  (let [{:keys [my-name my-email]}
        @(rf/subscribe [::subs/personal])]
    [chota/nav {:style {:margin-bottom 10}}
     [chota/nav-section {:left true}
      [chota/nav-link-brand {:href "#/cv"
                             :utils [:hide-xs]}
       [:img {:class (utils/with-classes "is-rounded")
              :src   (utils/with-gravatar-url my-email)
              :style {:margin-right 5 :padding-right 0}
              :alt   "Gravatar should be displayed here"}]
       [:div {:class (utils/with-classes "hide-sm" "hide-md")} my-name]]
      [chota/tabs {} (for [section [:cv :projects]]
                       ^{:key (utils/string-format "section-%s" section)}
                       [chota/tab
                        {:active? (= active-nav section)
                         :href (utils/string-format "#/%s" (name section))}
                        (case section :cv "CV" :projects "Projects")])]]]))
