(ns jedi2light.gitlab.io.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History]
           [goog.history EventType])
  (:require [secretary.core :as secretary]
            [goog.events :as gevents]
            [re-frame.core :as re-frame]
            [jedi2light.gitlab.io.events :as events]))

(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")

  ;; --- Routes -----------------------------------------------------------------------
  (defroute "/" []
    (re-frame/dispatch [::events/set-active-section :cv]))

  (defroute "/cv" []
    (re-frame/dispatch [::events/set-active-section :cv]))

  (defroute "/projects" []
    (re-frame/dispatch [::events/set-active-section :projects]))

  (defroute "/projects/:project-id" [project-id]
    (re-frame/dispatch [::events/set-active-section :projects-item])
    (re-frame/dispatch [::events/set-active-section-params {:project-id project-id}]))

  ;; ---------------------------------------------------------------------------------

  (hook-browser-navigation!))
