(ns jedi2light.gitlab.io.events
  (:require [jedi2light.gitlab.io.db :as db]
            [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [re-frame.core :refer [reg-event-fx]]
            [day8.re-frame.http-fx]
            [day8.re-frame.tracing :refer-macros [fn-traced]]))

(re-frame/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-section
 (fn-traced [db [_ active-section]]
   (assoc-in db [:active-section] active-section)))

(re-frame/reg-event-db
  ::set-active-section-params
  (fn-traced [db [_ active-section-params]]
   (assoc-in db [:active-section-params] active-section-params)))

(re-frame/reg-event-db
  ::set-gitlab-projects-filter-word
  (fn-traced [db [_ gl-projects-filter-word]]
   (assoc-in db [:projects :filter-word] gl-projects-filter-word)))

(re-frame/reg-event-fx
  ::load-gitlab-projects
  (fn-traced [{db :db} [_]]
   {:http-xhrio
     {:method          :get
      :uri             "https://gitlab.com/api/v4/users/jedi2light/projects"
      :params          {:visibility :public
                        :order_by   :last_activity_at
                        :owned      true
                        :simple     true
                        :per_page   20}
      :headers         nil
      :format          (ajax/json-request-format)
      :response-format (ajax/json-response-format {:keywords? true})
      :on-success      [::gitlab-success]
      :on-failure      [::gitlab-failure]}
    :db                (assoc-in db [:projects :gitlab] {:loading? true})}))

(re-frame/reg-event-db
  ::gitlab-success
  (fn-traced [db [_ results]]
   (assoc-in db [:projects :gitlab]
     {:loading?  false
      :error?    false
      :results   (map
                   (fn [item]
                     (let [{:keys [id name description web_url]} item]
                       {:id id :name name :description description
                        :links [{:on :gitlab :link web_url}]})) results)})))

(re-frame/reg-event-db
  ::gitlab-failure
  (fn-traced [db [_ results]]
   (assoc-in db [:projects :gitlab]
     {:loading?  false
      :error?    true
      :results   nil})))

(re-frame/reg-event-db
  ::gitlab-repo-file-success
  (fn-traced [db [_ data res]]
   (assoc-in
     db
     [:gitlab-repo-file (:project-id data) (:file-path data)]
     {:loading? false
      :error?   false
      :result   res})))

(re-frame/reg-event-db
  ::gitlab-repo-file-failure
  (fn-traced [db [_ data ___]]
   (assoc-in
     db
     [:gitlab-repo-file (:project-id data) (:file-path data)]
     {:loading? false
      :error?   true
      :result   nil})))

(re-frame/reg-event-fx
  ::load-gitlab-repo-file-content
  (fn-traced [{db :db} [_ project-id file-path]]
   {:db                (assoc-in db [:gitlab-repo-file project-id file-path] {:loading? true})
    :http-xhrio
     {:method          :get
      :uri             (str
                         "https://gitlab.com/api/v4/projects/"
                         project-id
                         "/repository/files/"
                         file-path)
      :params          {:ref "master"}
      :headers         nil
      :format          (ajax/json-request-format)
      :response-format (ajax/json-response-format {:keywords? true})
      :on-success      [::gitlab-repo-file-success {:project-id project-id :file-path file-path}]
      :on-failure      [::gitlab-repo-file-failure {:project-id project-id :file-path file-path}]}}))
