(ns jedi2light.gitlab.io.layout.naved
  (:require [jedi2light.gitlab.io.chota :as chota]
            [jedi2light.gitlab.io.blocks.navbar :as navbar]
            [jedi2light.gitlab.io.blocks.footer :as footer]))

(defn layout [{:keys [active-nav]} & children]
  [chota/grid {:container true}
   [navbar/component {:active-nav active-nav}] (into [:<>] children) [footer/component]])
