(ns jedi2light.gitlab.io.utils
  (:require [goog.date :as date]
            [goog.crypt :as crypt]
            [goog.crypt.Md5 :as Md5]
            [goog.string :as string]
            [goog.string.format]
            [clojure.string :as str]))

;; NOTE: Idea'll mark date and Md5 as unused requirements
;;       but they are NOT unused, they're actually needed

(defn target-val [ev]
  "Returns event target value"
  (aget ev "target" "value"))

(defn with-classes [& class-names]
  "Returns a class names string"
  (str/join " " (filter some? class-names)))

(defn with-md5-hash [string]
  "Returns MD5 hash for a string"
  (let [string->bytes (crypt/stringToUtf8ByteArray string)
        hash-instance (goog.crypt.Md5.)]
    (.update hash-instance string->bytes)
    (crypt/byteArrayToHex (.digest hash-instance))))

(defn with-current-year []
  "Returns a current year as Integer"
  (.getFullYear (goog.date.DateTime.)))

(defn string-format [string & format-args]
  "Simple wrapper for 'goog.string.format'"
  (apply string/format string format-args))

(defn with-title [keyword]
  "Returns a title from keyword"
  (str/join " " (map str/capitalize (str/split (name keyword) #"-"))))

(defn lower-case-includes? [string substr]
  "includes? with lower-case"
  (when (and (some? string) (some? substr))
    (str/includes? (str/lower-case string) (str/lower-case substr))))

(defn with-gravatar-url [email & {:keys [size] :or {size 150}}]
  "Returns Gravatar URL for specified email && size (default to 150)"
  (string-format
    "https://www.gravatar.com/avatar/%s?size=%d" (with-md5-hash email) size))

