(ns jedi2light.gitlab.io.subs
  (:require [re-frame.core :as re-frame]
            [jedi2light.gitlab.io.utils :as utils]))

(re-frame/reg-sub
 ::active-section
 (fn [db _]
   (get-in db [:active-section])))

(re-frame/reg-sub
  ::active-section-params
  (fn [db _]
    (get-in db [:active-section-params])))

(re-frame/reg-sub
  ::personal
  (fn [db _]
    (get-in db [:personal])))

(defn- get-by-id [vec id]
  (first
    (filter
      (fn [vec-item]
        (= (:id vec-item) (.parseInt js/Number id))) vec)))

(re-frame/reg-sub
  ::gitlab-projects-filter-word
  (fn [db _]
    (get-in db [:projects :filter-word])))

(defn- filter-projects [projects]
  (let [filter-word
        @(re-frame/subscribe
           [::gitlab-projects-filter-word])]
    (if (seq filter-word)
      (filter
        (fn [item]
          (or (utils/lower-case-includes?
                (:name item) filter-word)
              (utils/lower-case-includes?
                (:description item) filter-word))) projects)
      projects)))

(re-frame/reg-sub
  ::projects
  (fn [db [_ & [project-id]]]
    (let [projects
          (filter-projects (get-in db [:projects
                                       :gitlab :results]))]
      (if project-id (get-by-id projects project-id) projects))))

(re-frame/reg-sub
  ::gitlab-repo-file-error?
  (fn [db [_ & [project-id file-path]]]
    (get-in db [:gitlab-repo-file project-id file-path :error?])))

(re-frame/reg-sub
  ::gitlab-repo-file-loading?
  (fn [db [_ & [project-id file-path]]]
    (get-in db [:gitlab-repo-file project-id file-path :loading?])))

(re-frame/reg-sub
  ::gitlab-repo-file-content
  (fn [db [_ & [project-id file-path]]]
    (get-in db [:gitlab-repo-file project-id file-path :result :content])))
