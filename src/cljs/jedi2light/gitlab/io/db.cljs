(ns jedi2light.gitlab.io.db)

(def default-db
  (merge
    {;; system
     :active-section :cv
     :active-section-params  nil

     ;; personal data like: name, email, nickname and social networks
     :personal {:my-name     "Carey 🏳️‍⚧️"
                :my-email    "jedi2light@jedi2light.moe"
                :my-nickname "jedi2light"
                :my-networks [{:id :telegram
                               :link "https://t.me/jedi2light"}
                              {:id :gitlab
                               :link "https://gitlab.com/jedi2light"}
                              {:id :github
                               :link "https://github.com/jedi2light"}]}

     ;; loaded resources like: gitlab projects or gitlab project readme
     :projects {:gitlab      nil
                :filter-word nil}
     :gitlab-repo-file       nil}))
