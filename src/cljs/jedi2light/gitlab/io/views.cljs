(ns jedi2light.gitlab.io.views
  (:require [re-frame.core :as re-frame]
            [jedi2light.gitlab.io.subs :as subs]
            [jedi2light.gitlab.io.pages.cv :as cv]
            [jedi2light.gitlab.io.pages.projects :as projects]
            [jedi2light.gitlab.io.pages.projects-item :as projects-item]
            [jedi2light.gitlab.io.pages.trap :as trap]))

(defn- section-dispatcher [{:keys [active]}]
  (case active
    :cv [cv/page]
    :projects [projects/page]
    :projects-item [projects-item/page]
    :else [trap/page]))

(defn section-controller []
  [section-dispatcher {:active @(re-frame/subscribe [::subs/active-section])}])
