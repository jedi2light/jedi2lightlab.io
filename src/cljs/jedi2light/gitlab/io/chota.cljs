(ns jedi2light.gitlab.io.chota
  (:require [jedi2light.gitlab.io.utils :as utils]))

(defn- with-utils-classes [{:keys [utils] :as props}]
  (map name utils))

;; Grid

(defn grid
  [{:keys [reverse row container xs sm md lg] :as props}
   & children]
  (into
    [:div (merge
            {:class
             (apply
               utils/with-classes
               (concat
                (with-utils-classes props)
                [(when row "row")
                 (when reverse "reverse")
                 (when container "container")
                 (when xs (utils/string-format "col-%d" xs))
                 (when sm (utils/string-format "col-%d-sm" sm))
                 (when md (utils/string-format "col-%d-md" md))
                 (when lg (utils/string-format "col-%d-lg" lg))]))}
            (select-keys props [:style]))]
    children))

;; Buttons

(defn button
  [{:keys [color cleared? outlined? icon-only?] :as props}
   & children]
  (into
    [:button.button (merge
                     {:class (apply utils/with-classes
                                    (concat
                                     (with-utils-classes props)
                                     [(when color (name color))
                                      (when cleared? "clear")
                                      (when outlined? "outline")
                                      (when icon-only? "icon-only")]))}
                     (select-keys props [:style :on-click]))]
    children))

(defn link-button
  [{:keys [color cleared? outlined? icon-only?] :as props}
   & children]
  (into
    [:a.button      (merge
                     {:class (apply utils/with-classes
                                    (concat
                                     (with-utils-classes props)
                                     [(when color (name color))
                                      (when cleared? "clear")
                                      (when outlined? "outline")
                                      (when icon-only? "icon-only")]))}
                     (select-keys props  [:style :href :target :rel]))]
    children))

;; Nav

(defn nav
  [props & children]
  (into
    [:nav.nav       (merge {:class (apply utils/with-classes
                                          (with-utils-classes  props))}
                           (select-keys props [:style]))]
    children))

(defn nav-link-brand
  [props & children]
  (into
    [:a.brand       (merge {:class (apply utils/with-classes
                                          (with-utils-classes  props))}
                           (select-keys props [:style :href :target]))]
    children))

(defn nav-section
  [{:keys [left right center] :as props} & children]
  (into
    [:div           (merge {:class (apply
                                      utils/with-classes
                                      (concat
                                        (with-utils-classes props)
                                        [(when left "nav-left")
                                         (when right "nav-right")
                                         (when center "nav-center")]))}
                           (select-keys props [:style]))]
    children))

;; Tabs

(defn tabs
  [props & children]
  (into
    [:div.tabs      (merge {:class (apply
                                     utils/with-classes
                                     (with-utils-classes props))}
                           (select-keys props [:style]))]
    children))

(defn tab
  [{:keys [active?] :as props} & children]
  (into
    [:a             (merge {:class (apply
                                     utils/with-classes
                                     (concat
                                       (with-utils-classes props)
                                       [(when active? "active")]))}
                           (select-keys props [:style :href]))]
    children))

;; Card

(defn card
  [props & children]
  (into
    [:div.card      (merge {:class (apply utils/with-classes
                                          (with-utils-classes  props))}
                           (select-keys props [:style]))]
    children))
