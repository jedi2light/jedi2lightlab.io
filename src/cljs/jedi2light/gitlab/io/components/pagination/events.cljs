(ns jedi2light.gitlab.io.components.pagination.events
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-event-db
  ::set-current-page-number
  (fn [db [_ panel-id page-number]]
    (assoc-in db [:pagination panel-id :current-page-number] page-number)))
