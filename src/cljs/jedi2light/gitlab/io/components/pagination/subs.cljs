(ns jedi2light.gitlab.io.components.pagination.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
  ::current-page-number
  (fn [db [_ panel-id]]
    (get-in db [:pagination panel-id :current-page-number] 0)))
