(ns jedi2light.gitlab.io.components.pagination
  (:require [re-frame.core :as rf]
            [jedi2light.gitlab.io.utils :as utils]
            [jedi2light.gitlab.io.chota :as chota]
            [jedi2light.gitlab.io.components.pagination.subs :as subs]
            [jedi2light.gitlab.io.components.pagination.events :as events]))

(defn component [{:keys [panel-id pg-count]}]
  (let [pg-current
        @(rf/subscribe [::subs/current-page-number panel-id])]
    [:<>
     (->> pg-count range
          (map
            (fn [pg-number]
              ^{:key
                (utils/string-format "pag-%s-%d" panel-id pg-number)}
              [chota/button
               {:color :primary :outlined? (not (= pg-number pg-current))
                :on-click #(rf/dispatch [::events/set-current-page-number panel-id pg-number])} (inc pg-number)])))]))
