(ns jedi2light.gitlab.io.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [jedi2light.gitlab.io.views :as views]
            [jedi2light.gitlab.io.routes :as routes]
            [jedi2light.gitlab.io.events :as events]
            [jedi2light.gitlab.io.config :as config]))

(defn dev-setup []
  (when config/debug?
    (println "Carey, you are running you precious web site in development mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/section-controller] (.getElementById js/document "app")))

(defn init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (re-frame/dispatch-sync [::events/load-gitlab-projects])
  (dev-setup)
  (mount-root))
