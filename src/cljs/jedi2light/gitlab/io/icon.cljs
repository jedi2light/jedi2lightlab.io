(ns jedi2light.gitlab.io.icon
  (:require [jedi2light.gitlab.io.utils :as utils]
            [goog.string.format]))

(defn icon-gram-svg [{:keys [icon provider size color spacing] :as props} & children]
  (let [provider-str (name provider) icon-str (name icon)]
    [:img
     {:style {:margin-right spacing}
      :alt   (utils/string-format "%s-%s-%d" provider-str icon-str size)
      :src   (utils/string-format "https://icongr.am/%s/%s.svg?size=%d" provider-str icon-str size)}]))
